namespace AssigmentWAD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOderDetail1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "unitPrice", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetails", "unitPrice");
        }
    }
}
