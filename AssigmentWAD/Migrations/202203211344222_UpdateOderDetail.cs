namespace AssigmentWAD.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOderDetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderDetails", "quantity", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderDetails", "quantity");
        }
    }
}
