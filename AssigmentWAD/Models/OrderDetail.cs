﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssigmentWAD.Models
{
    public class OrderDetail
    {
        public int ID { get; set; }
        public virtual Order order { get; set; }

        public int quantity { get; set; }
        public virtual Product product { get; set; }
        public double unitPrice { get; set; }
        public int ProductId { get; set; }
        public int OrderId { get; set; }
    }
}