﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssigmentWAD.Models
{
    public class ShoppingCart
    {
        public Order order { get; set; }
        public IEnumerable<OrderDetail> OrderDetails { get; set; }
    }
}