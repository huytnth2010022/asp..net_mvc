﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssigmentWAD.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string thumbnail { get; set; }
        public virtual Category category { get; set; }
        public int CategoryId { get; set; }
    }
}