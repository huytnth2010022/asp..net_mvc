﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssigmentWAD.Models
{
    public class Order
    {
        public int ID { get; set; }
        public double total { get; set; }
        public virtual IEnumerable<OrderDetail> OrderDetail { get; set; }

    }
}