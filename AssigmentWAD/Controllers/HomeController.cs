﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using AssigmentWAD.Data;
using AssigmentWAD.Models;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace AssigmentWAD.Controllers
{
    public class HomeController : Controller
    {
        private Context db = new Context();
        private Order order;
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.category);
            return View(products.ToList());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult search(string key)
        {
            IOrderedQueryable<Product> model = db.Products;
            if (!string.IsNullOrEmpty(key))
            {
                model = model.Where(x => x.Name.Contains(key)).OrderBy(x => x.Price);
            }
            return PartialView(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult searchCategory(string category_id)
        {
            IOrderedQueryable<Product> model = db.Products;
            if (!string.IsNullOrEmpty(category_id))//DK 1
            {
                var caid = Int32.Parse(category_id);
                model = model.Where(x => x.CategoryId.Equals(caid)).OrderBy(x => x.Name);
            }
            return PartialView(model);
        }
        [HttpGet]
        public void GetOrder()
        {
            if (Session["giohang"] != null && Session["order"] != null)
            {
                List<OrderDetail> OrderDetails = Session["giohang"] as List<OrderDetail>;
                Order order = Session["order"] as Order;
            
                using (DbContextTransaction dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                         db.Orders.Add(order);
                        db.SaveChanges();
                        OrderDetails.ForEach(s => s.OrderId = order.ID);
                        foreach (var orderDetail in OrderDetails)
                        {
                            OrderDetail orderDetaildb = new OrderDetail()
                            {
                                ProductId  = orderDetail.ProductId,
                                OrderId = orderDetail.OrderId,
                                quantity = orderDetail.quantity,
                                unitPrice = orderDetail.unitPrice,
                            };
                            db.OrderDetails.Add(orderDetaildb);
                            db.SaveChanges();
                        }
  
                        dbTran.Commit();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        dbTran.Rollback();
                        throw;
                    }
                }
            }
            else
            {    
                        throw new InvalidOperationException("Order Fail!!");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Reduced(string id)
        {
            List<OrderDetail> OrderDetails = Session["giohang"] as List<OrderDetail>;
            order = Session["order"] as Order;
            int ID = Int32.Parse(id);
            OrderDetail orderDetail = OrderDetails.FirstOrDefault(m => m.ProductId == ID);
            if (orderDetail != null)
            {
                orderDetail.quantity--;
                if (orderDetail.quantity <= 0)
                {
                    OrderDetails.Remove(orderDetail);
                }
                orderDetail.unitPrice = orderDetail.quantity * orderDetail.product.Price;
            }
            order.total = 0;
            foreach (var item in OrderDetails)
            {
                order.total += item.unitPrice;
            }
            ShoppingCart shoppingcart = new ShoppingCart()
            {
                order = order,
                OrderDetails = OrderDetails,
            };
            return PartialView("AddCart", shoppingcart);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Plus(string id)
        {
            List<OrderDetail> OrderDetails = Session["giohang"] as List<OrderDetail>;
            order = Session["order"] as Order;
            int ID = Int32.Parse(id);
            OrderDetail orderDetail = OrderDetails.FirstOrDefault(m => m.ProductId == ID);
            if (orderDetail != null)
            {
                orderDetail.quantity++;
                orderDetail.unitPrice = orderDetail.quantity * orderDetail.product.Price;
            }
            order.total = 0;
            foreach (var item in OrderDetails)
            {
                order.total += item.unitPrice;
            }
            ShoppingCart shoppingcart = new ShoppingCart()
            {
                order = order,
                OrderDetails = OrderDetails,
            };
            return PartialView("AddCart", shoppingcart);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult DeleteCart(string id)
        {
            List<OrderDetail> OrderDetails = Session["giohang"] as List<OrderDetail>;
            order = Session["order"] as Order;
            int ID = Int32.Parse(id);
            OrderDetail orderDetaildelete = OrderDetails.FirstOrDefault(m => m.ProductId == ID);
            if (orderDetaildelete != null)
            {
                OrderDetails.Remove(orderDetaildelete);
            }
            order.total = 0;
            foreach (var item in OrderDetails)
            {
            order.total += item.unitPrice;
            }

            ShoppingCart shoppingcart = new ShoppingCart()
            {
                order = order,
                OrderDetails = OrderDetails,
            };
            return PartialView("AddCart",shoppingcart);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult AddCart(string id)
        {
            if (Session["giohang"] == null && Session["order"] == null) 
            {
                Session["giohang"] = new List<OrderDetail>();
                Session["order"] = new Order();
              
            }

            order = Session["order"] as Order;
            List<OrderDetail> orderDetails = Session["giohang"] as List<OrderDetail>;
            var ProductID = Int32.Parse(id);
            if (orderDetails.FirstOrDefault(m => m.ProductId == ProductID)==null)
            {
                OrderDetail orderDetail = new OrderDetail()
                {
                    ProductId = ProductID,
                    quantity = 1,
                    product = db.Products.Find(ProductID),
                    OrderId = order.ID,
                };
                orderDetail.unitPrice = orderDetail.product.Price * orderDetail.quantity;
                order.total = order.total + orderDetail.quantity * orderDetail.product.Price;
                orderDetails.Add(orderDetail);
            }
            else
            {
              
              OrderDetail orderDetail = orderDetails.FirstOrDefault(m => m.ProductId == ProductID);
               int oldquantity = orderDetail.quantity;
                orderDetail.quantity++;
                order.total = order.total + orderDetail.product.Price * (orderDetail.quantity - oldquantity);
                orderDetail.unitPrice = orderDetail.product.Price * orderDetail.quantity;
            }
            ShoppingCart shoppingcart = new ShoppingCart()
            {
                order = order,
                OrderDetails = orderDetails,
            };
            return PartialView(shoppingcart);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}