﻿using Authencation_Demo.Data;
using Authencation_Demo.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Authencation_Demo.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        private MyIdentittyContext myIdentittyContext;
        private UserManager<User> userManager;
        public UserController()
        {
            myIdentittyContext = new MyIdentittyContext();
            UserStore<User> userStore = new UserStore<User>(myIdentittyContext);
            userManager = new UserManager<User>(userStore);
        }

        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(string UserName, string PasswordHash)
        {
            var user = await userManager.FindAsync(UserName, PasswordHash);
            if (user == null)
            {

                return View("Failed");
            }
            else
            {
                SignInManager<User, string> signInManager
                    = new SignInManager<User, string>(userManager, Request.GetOwinContext().Authentication);
                await signInManager.SignInAsync(user, false, false);
                return View("Successful");
            }
        }
        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<ActionResult> Register(User user)
        {
            User userModel = new User()
            {
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
        RollNumber = user.RollNumber,
         Email = user.Email,
         Phone = user.Phone,
    };
            var result = await userManager.CreateAsync(userModel, user.PasswordHash);
            if (result.Succeeded)
            {
                return View("Successful");
            }
            else
            {
                return View("Failed");
            }
        }
        public ActionResult Home()
        {
            return View();
        }
    }
}