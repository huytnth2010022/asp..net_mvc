namespace Authencation_Demo.Migrations
{
    using Authencation_Demo.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Authencation_Demo.Data.MyIdentittyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Authencation_Demo.Data.MyIdentittyContext context)
        {
            var roles = new List<Role>
            {
                new Role{Name = "Employee"},
                new Role{Name = "User"},
                new Role{Name = "Admin"}
            };
            roles.ForEach(s => context.Roles.Add(s));
            context.SaveChanges();
        }
    }
}
