﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authencation_Demo.Models
{
    public class User : IdentityUser
    {
        public User()
        {
            this.Status = 0;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RollNumber { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }

       
    }
  
}