﻿namespace Thi_ASP_NET.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Thi_ASP_NET.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Thi_ASP_NET.Data.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Thi_ASP_NET.Data.Context context)
        {
            var products = new List<Product>
            {
                new Product { ProductName = "Kiếm Thần Majumu",   SupplierId = 1, CategoryId=1, QuantityPerUnit=12, UnitPrice=10000, UnitslnStock=10000,UnitsOnOrder=1000, ReorderLevel=1,Discontinued=1  },
                new Product { ProductName = "Rìu Tiamac", SupplierId =2, CategoryId=2 , QuantityPerUnit=13, UnitPrice=20000, UnitslnStock=20000,UnitsOnOrder=1000, ReorderLevel=2,Discontinued=1},
                new Product { ProductName = "Kiếm B.F",   SupplierId = 3, CategoryId=3 , QuantityPerUnit=14, UnitPrice=30000, UnitslnStock=10000,UnitsOnOrder=1000, ReorderLevel=1,Discontinued=1},
                new Product { ProductName = "Súng Hải tặc",    SupplierId = 4, CategoryId=4, QuantityPerUnit=15, UnitPrice=40000, UnitslnStock=40000,UnitsOnOrder=1000, ReorderLevel=1,Discontinued=0},
            };
            products.ForEach(s => context.Product.AddOrUpdate(s));

            var categories = new List<Category>
            {
                new Category { CategoryName = "Vật lý", Description="ABC1", Picture="Anh1" },
                new Category { CategoryName = "Sức Mạng phép thuật", Description="ABC2", Picture="Anh2"},
                new Category { CategoryName = "Chống chịu", Description="ABC3", Picture="Anh3"},
                new Category { CategoryName = "Sát lực", Description="ABC4", Picture="Anh4"},
            };
            categories.ForEach(s => context.Category.AddOrUpdate(s));
            context.SaveChanges();
        }
    }
}
