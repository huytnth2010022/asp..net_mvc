﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Thi_ASP_NET.Models;

namespace Thi_ASP_NET.Data
{
    public class Context : DbContext
    {
        public Context() : base("AssigmentWAD")
        {

        }
        public DbSet<Product> Product { get; set; }
        public DbSet<Category> Category { get; set; }
    }
}